import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useRouteMatch
} from "react-router-dom";

import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/users">Users</Link>
              </li>
            </ul>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/:device" >
              <Device />
            </Route>
            <Route path="/">
              {/* <Redirect to="/d/zh-HK/sports" /> */}
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

const Device = () => {
  const { url, params: { device } } = useRouteMatch();

  return (
    <>
      <div>Device is {device}</div>
      <Switch>
        <Route path={`${url}/:language`} >
          <Language />
        </Route>
      </Switch>
    </>
  )
}

const Language = () => {
  const { url, params: { language } } = useRouteMatch();

  return (
    <>
      <div>Language is {language}</div>
      <Switch>
        <Route path={`${url}/sports`}>
          <Sports />
        </Route>
        <Route path={`${url}/:category`} >
          <Category />
        </Route>
      </Switch>
    </>
  )
}

const Sports = () => {
  const { url } = useRouteMatch();

  return (
    <>
      <div>Sports</div>
      <Switch>
        <Route path={`${url}/:sport/list/:markets/:page?`}>
          <SportsList />
        </Route>
        <Route path={`${url}/:sport/details/:id`} >
          <SportsDetails />
        </Route>
      </Switch>
    </>
  )
}

const SportsList = () => {
  const { params: { sport, markets } } = useRouteMatch();

  return (
    <>
      <div>Sports for {sport} ({markets})</div>
      <ul>
        <li>item 1</li>
        <li>item 2</li>
        <li>item 3</li>
        <li>item 4</li>
      </ul>
    </>
  )
}

const SportsDetails = () => {
  const { params: { sport, id } } = useRouteMatch();

  return (
    <>
      <div>Sports Details for {sport} ID {id}</div>
      <ul>
        <li>item 1</li>
        <li>item 2</li>
        <li>item 3</li>
        <li>item 4</li>
      </ul>
    </>
  )
}

const Category = () => {
  const { params: { category } } = useRouteMatch();

  return (
    <>
      <div>Category</div>
    </>
  )
}

export default App;
